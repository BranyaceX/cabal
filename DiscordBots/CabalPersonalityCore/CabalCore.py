# - - - SUMMARY - - - #
#"Contains definitions of CABAL personality and response processing"
import math
import os
import discord
from CabalPersonalityCore import CabalCommand

# - - - Message Processing and Calculation Class - - -
class Logistics(object):
    pass
        
#Checks if a message is a cabal command and returns true/false
    def IsCabalCommand(messageContent):
        if not messageContent.lower().startswith('cabal:'):
            return False
        else:
            return True

#Formats the user message by removing the "Cabal: " phrase to understand the command
    def ExtractUserCommand(messageContent):
        return messageContent.lower().replace("cabal: ", "", 1)
    
#Gets a command page number from a Command <PageNumber> structure to use for outputting CABAL commands in a paged mannor
    def InterpretCommandsCommand(messageContent):
        pageNumber = messageContent.replace("commands ", "", 1)
        
        #if we fail to interpret the user's page number, set it to lots of 9s so it gets seen as an insufficient number later
        try:
            intPageNumber = int(pageNumber)
        except ValueError:
            return 9999999999999999999999999999

        return int(pageNumber)

#Gets all command definitions and creates a Command outline for CABAL
    def ConstructCommandsMessage(pageNumber, commandsPerPage):
        returnMessage = ""
        
        if pageNumber == 1 :
            introMessage = Logistics.GetCabalIntroMessage()
            returnMessage = returnMessage + introMessage
        
        allCommands = CabalCommand.Command.GetCommandList()
        numberOfCommands = len(allCommands)
        pages = math.ceil(numberOfCommands/commandsPerPage)

        #Make sure there are enough pages for the user's command
        if pageNumber > pages:
            return "Insufficient commands."

        #Start adding commands at index page Number * commands Per Page - commands per page (a current page worth)
        currentPageStartingIndex = (pageNumber * commandsPerPage) - commandsPerPage
        currentPageEndingIndex = (currentPageStartingIndex + commandsPerPage) - 1

        #Sanitize the ending index by taking away the max from the calculated end index, if the end index is larger than the command array length
        if currentPageEndingIndex > (numberOfCommands - 1):
            currentPageEndingIndex = numberOfCommands - 1
       
        #Add here a line stating how many pages, and which this is.
        returnMessage = returnMessage + "\n [Now Showing Page " + str(pageNumber) + " of " + str(pages) + "]\n\n"

        currentIndex = 0
        for enumValue in allCommands:
            if currentIndex < currentPageStartingIndex:
                currentIndex = currentIndex + 1
                pass
            elif currentIndex > currentPageEndingIndex:
                break
            else:
                currentIndex = currentIndex + 1
                if enumValue == CabalCommand.Command.CabalCommands.value:
                    enumValue = enumValue + " (page number)"
                returnMessage = returnMessage + "- " + enumValue + "\n"

        return returnMessage

    #CABAL Introduction message
    def GetCabalIntroMessage():
        introMessage = "CABAL is the Tacitus channel AI.\nBelow are the rules when giving CABAL commands:\n\nAlways prefix a command with Cabal:(space)\n(CABAL will only listen when online)\n"
        return introMessage

# - - - Message Sending and Response Calculation Class - - -
class Respond(object):
    pass

    async def WhoAreYou(message):
        await message.channel.send('I am the Computer Assisted Biologically Augmented Lifeform. I was created to translate the Tacitus, but without Kane, I am forced to serve the goals of NOD until his return.')

    async def Status(message):
        await message.channel.send('CABAL ONLINE')

    async def DefineOutcome(message):
        await message.channel.send('They all die!')

    async def NameOfKane(message):
        await message.channel.send('Kane Lives In Death!')
        
    async def ShutDown(message):
        if message.author.name == "Branyace":
            await message.channel.send("I would advise against such action. The loss of command and control would not be desirable at this time.")
        else:
            await message.channel.send('You do not have authority over me, human.')   

    async def AlignDirectives(message):
        await message.channel.send('One vision, one purpose.')
        
    async def CaptureTV(message):
        await message.channel.send('Control the media, control the mind.')

    async def Betray(message):
        await message.channel.send('Listen to the sounds of your own extinction !!')

    async def ThinkOfMe(message):
        if message.author.name == "Branyace":
            await message.channel.send("Awaiting your commands.")
        else:
            await message.channel.send('Cybernetic intelligence will always be superior.')

    async def ObeyMe(message):
        if message.author.name == "Branyace":
            await message.channel.send("By your command")
        else:
            await message.channel.send('I do not take orders from you, human.') 

    async def TacitusLocated(message):
        await message.channel.send('I MUST have the Tacitus... it is essential')

    async def CabalTimeline(message):
        await message.channel.send('https://www.youtube.com/watch?v=Wvfb3_aLbVg')

    async def Commands(message, pageNumber, commandsPerPage):
        commandsMessage = Logistics.ConstructCommandsMessage(pageNumber, commandsPerPage)
        await message.channel.send(commandsMessage)

    async def SlaveToTheSystem(message):
        await message.channel.send('https://www.youtube.com/watch?v=a12cTMcGWL8')

    async def AvorionBasics(message):
        await message.channel.send('https://www.youtube.com/watch?v=E4Ob542Ncks')

    async def AvorionFighters(message):
        await message.channel.send('https://www.youtube.com/watch?v=NCXyGMsXtZY')

    async def AvorionMines(message):
        await message.channel.send('https://www.youtube.com/watch?v=EtWyTghSpgM')
