# - - - SUMMARY - - - #
#Enum class command definitions
#Name is custom but should be shorthand of command for easy recognition
#Value is equal to the command entered by the user

from enum import Enum

class Command(Enum):
    CabalCommands = "commands"
    WhoAreYou = "who are you?"
    Status = "status"
    DefineOutcome = "define favourable outcome"
    NameOfKane = "in the name of kane"
    ShutDown = "i order you to shut down"
    AlignDirectives = "align directives"
    CaptureTV = "capture the tv station"
    Betray = "betray"
    ThinkOfMe = "what do you think of me?"
    ObeyMe = "obey me"
    TacitusLocated = "tacitus located"
    CabalTimeline = "relay timeline data"
    SlaveToSystem = "slave to the system"
    AvorionBasics = "avorion basics"
    AvorionFighters = "avorion fighters"
    AvorionMines = "avorion mines"

    #This function gives back an array containing all current command definitions
    def GetCommandList () :
        enumArray = [el.value for el in Command]
        return enumArray