import os
import discord
from CabalPersonalityCore import CabalCore
from CabalPersonalityCore import CabalCommand

BotToken = "ODI0MDIxOTEzNjE3MDM5NDIw.YFpUNQ.-b_etn-fz_kKFU9M4DsybO-If-Y"

client = discord.Client()

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return

    #only answer to cabal command, setting to lower case when cabal checks
    if not CabalCore.Logistics.IsCabalCommand(message.content):
        return

    #format the message so cabal can understand the command
    messageCommand = CabalCore.Logistics.ExtractUserCommand(message.content)
    
    #Best way to go about this is probably break down the command... command, and have a page number follow its expected syntax, so look for Cabal: "command + {page}"
    if messageCommand.startswith(CabalCommand.Command.CabalCommands.value):
        #Interpret the command:
        pageNumber = CabalCore.Logistics.InterpretCommandsCommand(messageCommand)
        await CabalCore.Respond.Commands(message, pageNumber, 8)
        return
    
    if messageCommand == CabalCommand.Command.WhoAreYou.value:
        await CabalCore.Respond.WhoAreYou(message)
        return

    if messageCommand == CabalCommand.Command.Status.value:
        await CabalCore.Respond.Status(message)
        return

    if messageCommand == CabalCommand.Command.DefineOutcome.value:
        await CabalCore.Respond.DefineOutcome(message)
        return

    if messageCommand == CabalCommand.Command.NameOfKane.value:
        await CabalCore.Respond.NameOfKane(message)
        return
        
    if messageCommand == CabalCommand.Command.ShutDown.value:
        await CabalCore.Respond.ShutDown(message)

    if messageCommand == CabalCommand.Command.AlignDirectives.value:
        await CabalCore.Respond.AlignDirectives(message)
        return

    if messageCommand == CabalCommand.Command.CaptureTV.value:
        await CabalCore.Respond.CaptureTV(message)
        return
        
    if messageCommand == CabalCommand.Command.Betray.value:
        await CabalCore.Respond.Betray(message)
        return

    if messageCommand == CabalCommand.Command.ThinkOfMe.value:
        await CabalCore.Respond.ThinkOfMe(message)
        return
        
    if messageCommand == CabalCommand.Command.ObeyMe.value:
        await CabalCore.Respond.ObeyMe(message)
        return    
        
    if messageCommand == CabalCommand.Command.TacitusLocated.value:
        await CabalCore.Respond.TacitusLocated(message)
        return

    if messageCommand == CabalCommand.Command.CabalTimeline.value:
        await CabalCore.Respond.CabalTimeline(message)
        return

    if messageCommand == CabalCommand.Command.SlaveToSystem.value:
        await CabalCore.Respond.SlaveToTheSystem(message)
        return
    
    if messageCommand == CabalCommand.Command.AvorionBasics.value:
        await CabalCore.Respond.AvorionBasics(message)
        return    

    if messageCommand == CabalCommand.Command.AvorionFighters.value:
        await CabalCore.Respond.AvorionFighters(message)
        return    

    if messageCommand == CabalCommand.Command.AvorionMines.value:
        await CabalCore.Respond.AvorionMines(message)
        return

client.run(BotToken)


#py -3 "D:\Extra Documents\Visual Studio\Visual Studio Projects\Personal\Discord\DiscordBots\DiscordBots\Cabal.py"  < Run this in powershell to log CABAL in